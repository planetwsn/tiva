#include <stdint.h>
#include <stdbool.h>

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/sysctl.h"
#include "driverlib/adc.h"
#include "driverlib/gpio.h"

#define SAMPLE_SEQ_3    3
#define TOP_PRIORITY    0
#define SAMPLE0         0

/**
 * main.c
 */
int main(void)
{
    uint32_t rawData;

    //1.Configure system clock
    SysCtlClockSet(SYSCTL_OSC_MAIN | SYSCTL_USE_PLL); //1MHz

    //2. Enable ADC peripherals
    SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);

    //3. Select SS and sampling method
    ADCSequenceConfigure(ADC0_BASE, SAMPLE_SEQ_3, ADC_TRIGGER_PROCESSOR, TOP_PRIORITY);

    //4. Configure sample sequencer
    ADCSequenceStepConfigure(ADC0_BASE, SAMPLE_SEQ_3, SAMPLE0, ADC_CTL_TS | ADC_CTL_IE | ADC_CTL_END);

    //5. Enable sample sequencer
    ADCSequenceEnable(ADC0_BASE, SAMPLE_SEQ_3);


    while(1){
        ADCIntClear(ADC0_BASE, SAMPLE_SEQ_3);
        ADCProcessorTrigger(ADC0_BASE, SAMPLE_SEQ_3);
        ADCSequenceDataGet(ADC0_BASE, SAMPLE_SEQ_3, &rawData);
    }
}
