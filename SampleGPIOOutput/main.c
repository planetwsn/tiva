#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_types.h"  //for translating hw register formats to their sw equivalent
#include "inc/hw_memmap.h" //for base addresses
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"

/**
 * main.c
 */
int main(void)
{
    //1. Set system clock for clock control of peripherals and registers
    SysCtlClockSet(SYSCTL_OSC_MAIN | SYSCTL_USE_PLL ); //osc main = 1MHz | Div 5 = 20MHz

    //2. Enable a GPIO port
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF); // choose GPIO port to enable

    //3. Set GPIO pins as output
    GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3);

    //done

    //Proceed with whatever function you want your configured pins to do...
    while(1){
        GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3, 2);
        SysCtlDelay(200000);
        GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3, 0);
        SysCtlDelay(200000);
    }
}
