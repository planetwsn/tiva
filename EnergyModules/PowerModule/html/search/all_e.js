var searchData=
[
  ['vectorize_5fsignal',['vectorize_signal',['../group__pm.html#ga26efba4814a31d11753389cfd6c45787',1,'pm.c']]],
  ['voff',['voff',['../structdata_f_d__t.html#a5399e97d514a3083be4bee6dc147b090',1,'dataFD_t']]],
  ['voltage',['voltage',['../structraw_data__t.html#a7a85c6ca8f4fc56138473f0bb510c68f',1,'rawData_t']]],
  ['voltage_5fchannel',['VOLTAGE_CHANNEL',['../group__pm.html#ga3ad11a109af35fa8de5e0b1b3e60142b',1,'pm.h']]],
  ['vrms',['vrms',['../structdata_t_d__t.html#a20b22adad93c02222c6b60f2ebecc198',1,'dataTD_t']]],
  ['vrms1',['vrms1',['../structdata_f_d__t.html#a6aafd62417a3f153a03864b13d5e7e6d',1,'dataFD_t']]]
];
