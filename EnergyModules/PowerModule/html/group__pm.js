var group__pm =
[
    [ "pm.c", "pm_8c.html", null ],
    [ "pm.h", "pm_8h.html", null ],
    [ "dataTD_t", "structdata_t_d__t.html", [
      [ "freq", "structdata_t_d__t.html#adb199ac806e31248b4350941b5927a6f", null ],
      [ "irms", "structdata_t_d__t.html#a9f52458aedff5c4945f56cc219d684b6", null ],
      [ "pfac", "structdata_t_d__t.html#accc7eb6b078e4542d59fade34bfce479", null ],
      [ "ppwr", "structdata_t_d__t.html#a8aa1de3288e17a79798c4d421e05a057", null ],
      [ "qpwr", "structdata_t_d__t.html#ace9fbdb9df4b59458d008db6d98a8089", null ],
      [ "spwr", "structdata_t_d__t.html#a7864a9d86440304de113e7b75fd9f98d", null ],
      [ "vrms", "structdata_t_d__t.html#a20b22adad93c02222c6b60f2ebecc198", null ]
    ] ],
    [ "dataFD_t", "structdata_f_d__t.html", [
      [ "harm", "structdata_f_d__t.html#ae53ff767dcf35dc50a4e6b64b5ca5ce2", null ],
      [ "ioff", "structdata_f_d__t.html#afabfd9b12a48f68394b29227784af80b", null ],
      [ "irms1", "structdata_f_d__t.html#aaace1a6c3e782c1cf6652085551024b9", null ],
      [ "voff", "structdata_f_d__t.html#a5399e97d514a3083be4bee6dc147b090", null ],
      [ "vrms1", "structdata_f_d__t.html#a6aafd62417a3f153a03864b13d5e7e6d", null ]
    ] ],
    [ "rawData_t", "structraw_data__t.html", null ],
    [ "__BSD_VISIBLE", "group__pm.html#gaa00daaab11d6072cf978ba8268041ed6", null ],
    [ "CURRENT_CHANNEL", "group__pm.html#ga501e37e951966e98511c9c1eb0c7d397", null ],
    [ "INITBOARD", "group__pm.html#gad0d6b1ad78bd661bd8b315dcf0a2f93e", null ],
    [ "SAMPLE_SEQUENCER_1", "group__pm.html#ga72b6e2cd5a6951427db3f69475dcebff", null ],
    [ "SAMPLESIZE", "group__pm.html#gaf675e47c038bc2a7f746eba51da99fe7", null ],
    [ "SAMPLING_RATE", "group__pm.html#ga17889cf1cf83a54524c242fa5a353cf1", null ],
    [ "VOLTAGE_CHANNEL", "group__pm.html#ga3ad11a109af35fa8de5e0b1b3e60142b", null ],
    [ "compute_fd", "group__pm.html#ga85de642b038fffb57d06d642e5defebd", null ],
    [ "compute_td", "group__pm.html#ga24de6e69923681e21471d5aa5fab1362", null ],
    [ "init_board", "group__pm.html#gad44ec0e3bb45edf653bd04c37508e269", null ],
    [ "sample_signal", "group__pm.html#gaca43e3e6a73564ac9902515e510d453d", null ],
    [ "vectorize_signal", "group__pm.html#ga26efba4814a31d11753389cfd6c45787", null ]
];