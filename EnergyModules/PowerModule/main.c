/**
 * \mainpage
 * This page documents the APIs available for the power measurement module that is
 * designed with Tiva C Launchpad Board (TM4C123G). You can also visit the Gitlab repository
 * of the codes documented herein through this link: https://gitlab.com/planetwsn/tiva.git
 *
 * \file main.c
 * \brief Main C file for the power module
 *
 * This file uses all the API available for measuring power using TM4C123GH6PM MCU.
 *
 * \author Mark Anthony Cabilo
 * \date February 2019
 * \version 1.0
 */
#include "pm.h"

/**
 * main function
 * \f$ \frac{x}{y} \f$
 */
int main(){
}
