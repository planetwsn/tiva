/**
 * \file pm.h
 * \brief Header file for the power module API
 *
 * This file contains the API prototypes and macros for the power measurement module
 *
 * \author Mark Anthony Cabilo
 * \date February 2019
 * \version 1.0
 */

#ifndef PM_H
#define PM_H

#include <stdio.h>
#include <stdlib.h>
#define __BSD_VISIBLE //enable some mathematical macros such as pi
#include <math.h>

/**
 * @struct dataTD_t
 * @brief Time-domain data structure
 */
typedef struct{
    double vrms;    //! Root-mean-square voltage
    double irms;    //! Root-mean-square current
    double freq;    //! Frequency (time-domain)
    double ppwr;    //! Active power
    double qpwr;    //! Reactive power
    double spwr;    //! Apparent power
    double pfac;    //! Power factor
}dataTD_t;

//! \struct dataFD_t
typedef struct{
    double vrms1;   //! Root-mean-square voltage of fundamental frequency
    double irms1;   //! Root-mean-square current of fundamental frequency
    double voff;    //! DC offset voltage
    double ioff;    //! DC offset current
    double harm[20];    //! Harmonics (fundamental + 19 harmonics)
}dataFD_t;


#endif //PM_H
