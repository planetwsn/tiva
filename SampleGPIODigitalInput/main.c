#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "inc/hw_ints.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/interrupt.h"


static uint32_t value;

//Include this ISR in the NVIC table found in tm4c123gh6pm_startup_ccs.c
void GPIOFIntHandler(void){
    GPIOIntClear(GPIO_PORTF_BASE, GPIO_INT_PIN_4);

    if(value == 8)
        value = 2;
    else
        value *= 2;

}

/**
 * main.c
 */
int main(void)
{
    //1. Configure system clock (main osc = 1MHz | Div 5 = 200kHz)
    SysCtlClockSet(SYSCTL_OSC_MAIN | SYSCTL_USE_PLL);

    //2. Enable GPIO Port F by providing the system clock to it
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);

    //3. Register the port-level ISR
    GPIOIntRegister(GPIO_PORTF_BASE, GPIOFIntHandler);

    //4. Configure PF4 as input
    GPIOPinTypeGPIOInput(GPIO_PORTF_BASE, GPIO_PIN_4);

    //4b. Enable pull-down resistors for buttons
    GPIOPadConfigSet(GPIO_PORTF_BASE, GPIO_PIN_4, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPU);

    //5. Configure PF1, PF2, and PF3 as output
    GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3);

    //6. Configure interrupt type
    GPIOIntTypeSet(GPIO_PORTF_BASE, GPIO_PIN_4, GPIO_RISING_EDGE);

    //6. Assign the interrupt pins
    GPIOIntEnable(GPIO_PORTF_BASE, GPIO_PIN_4);

    //7. Write whatever function your MCU should perform repeatedly
    value = 2;
    while(1){
        GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3, value);
        SysCtlDelay(200000);
        GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3, 0);
        SysCtlDelay(200000);
    }
}


