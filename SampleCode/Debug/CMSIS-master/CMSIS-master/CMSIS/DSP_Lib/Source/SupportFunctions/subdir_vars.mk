################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_copy_f32.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_copy_q15.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_copy_q31.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_copy_q7.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_fill_f32.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_fill_q15.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_fill_q31.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_fill_q7.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_float_to_q15.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_float_to_q31.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_float_to_q7.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_q15_to_float.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_q15_to_q31.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_q15_to_q7.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_q31_to_float.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_q31_to_q15.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_q31_to_q7.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_q7_to_float.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_q7_to_q15.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_q7_to_q31.c 

C_DEPS += \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_copy_f32.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_copy_q15.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_copy_q31.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_copy_q7.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_fill_f32.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_fill_q15.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_fill_q31.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_fill_q7.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_float_to_q15.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_float_to_q31.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_float_to_q7.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_q15_to_float.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_q15_to_q31.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_q15_to_q7.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_q31_to_float.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_q31_to_q15.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_q31_to_q7.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_q7_to_float.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_q7_to_q15.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_q7_to_q31.d 

OBJS += \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_copy_f32.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_copy_q15.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_copy_q31.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_copy_q7.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_fill_f32.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_fill_q15.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_fill_q31.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_fill_q7.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_float_to_q15.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_float_to_q31.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_float_to_q7.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_q15_to_float.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_q15_to_q31.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_q15_to_q7.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_q31_to_float.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_q31_to_q15.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_q31_to_q7.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_q7_to_float.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_q7_to_q15.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_q7_to_q31.obj 

OBJS__QUOTED += \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\SupportFunctions\arm_copy_f32.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\SupportFunctions\arm_copy_q15.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\SupportFunctions\arm_copy_q31.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\SupportFunctions\arm_copy_q7.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\SupportFunctions\arm_fill_f32.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\SupportFunctions\arm_fill_q15.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\SupportFunctions\arm_fill_q31.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\SupportFunctions\arm_fill_q7.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\SupportFunctions\arm_float_to_q15.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\SupportFunctions\arm_float_to_q31.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\SupportFunctions\arm_float_to_q7.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\SupportFunctions\arm_q15_to_float.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\SupportFunctions\arm_q15_to_q31.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\SupportFunctions\arm_q15_to_q7.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\SupportFunctions\arm_q31_to_float.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\SupportFunctions\arm_q31_to_q15.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\SupportFunctions\arm_q31_to_q7.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\SupportFunctions\arm_q7_to_float.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\SupportFunctions\arm_q7_to_q15.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\SupportFunctions\arm_q7_to_q31.obj" 

C_DEPS__QUOTED += \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\SupportFunctions\arm_copy_f32.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\SupportFunctions\arm_copy_q15.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\SupportFunctions\arm_copy_q31.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\SupportFunctions\arm_copy_q7.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\SupportFunctions\arm_fill_f32.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\SupportFunctions\arm_fill_q15.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\SupportFunctions\arm_fill_q31.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\SupportFunctions\arm_fill_q7.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\SupportFunctions\arm_float_to_q15.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\SupportFunctions\arm_float_to_q31.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\SupportFunctions\arm_float_to_q7.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\SupportFunctions\arm_q15_to_float.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\SupportFunctions\arm_q15_to_q31.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\SupportFunctions\arm_q15_to_q7.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\SupportFunctions\arm_q31_to_float.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\SupportFunctions\arm_q31_to_q15.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\SupportFunctions\arm_q31_to_q7.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\SupportFunctions\arm_q7_to_float.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\SupportFunctions\arm_q7_to_q15.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\SupportFunctions\arm_q7_to_q31.d" 

C_SRCS__QUOTED += \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_copy_f32.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_copy_q15.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_copy_q31.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_copy_q7.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_fill_f32.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_fill_q15.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_fill_q31.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_fill_q7.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_float_to_q15.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_float_to_q31.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_float_to_q7.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_q15_to_float.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_q15_to_q31.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_q15_to_q7.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_q31_to_float.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_q31_to_q15.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_q31_to_q7.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_q7_to_float.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_q7_to_q15.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/SupportFunctions/arm_q7_to_q31.c" 


