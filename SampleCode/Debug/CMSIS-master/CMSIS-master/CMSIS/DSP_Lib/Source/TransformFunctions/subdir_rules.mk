################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Each subdirectory must supply rules for building sources it contributes
CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_bitreversal.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_bitreversal.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_bitreversal.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_bitreversal2.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_bitreversal2.S $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_bitreversal2.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_f32.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_f32.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_f32.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_q15.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_q15.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_q15.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_q31.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_q31.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_q31.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_f32.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_f32.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_f32.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_init_f32.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_init_f32.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_init_f32.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_init_q15.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_init_q15.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_init_q15.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_init_q31.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_init_q31.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_init_q31.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_q15.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_q15.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_q15.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_q31.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_q31.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_q31.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_f32.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_f32.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_f32.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_init_f32.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_init_f32.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_init_f32.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_init_q15.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_init_q15.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_init_q15.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_init_q31.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_init_q31.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_init_q31.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_q15.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_q15.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_q15.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_q31.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_q31.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_q31.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix8_f32.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix8_f32.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix8_f32.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_f32.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_f32.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_f32.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_init_f32.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_init_f32.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_init_f32.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_init_q15.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_init_q15.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_init_q15.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_init_q31.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_init_q31.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_init_q31.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_q15.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_q15.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_q15.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_q31.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_q31.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_q31.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_f32.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_f32.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_f32.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_fast_f32.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_fast_f32.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_fast_f32.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_fast_init_f32.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_fast_init_f32.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_fast_init_f32.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_init_f32.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_init_f32.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_init_f32.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_init_q15.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_init_q15.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_init_q15.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_init_q31.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_init_q31.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_init_q31.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_q15.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_q15.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_q15.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_q31.obj: ../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_q31.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_q31.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/TransformFunctions" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '


