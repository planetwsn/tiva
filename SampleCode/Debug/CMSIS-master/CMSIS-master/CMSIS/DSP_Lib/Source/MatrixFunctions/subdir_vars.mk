################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_add_f32.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_add_q15.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_add_q31.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_cmplx_mult_f32.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_cmplx_mult_q15.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_cmplx_mult_q31.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_init_f32.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_init_q15.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_init_q31.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_inverse_f32.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_inverse_f64.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_mult_f32.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_mult_fast_q15.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_mult_fast_q31.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_mult_q15.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_mult_q31.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_scale_f32.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_scale_q15.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_scale_q31.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_sub_f32.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_sub_q15.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_sub_q31.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_trans_f32.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_trans_q15.c \
../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_trans_q31.c 

C_DEPS += \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_add_f32.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_add_q15.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_add_q31.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_cmplx_mult_f32.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_cmplx_mult_q15.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_cmplx_mult_q31.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_init_f32.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_init_q15.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_init_q31.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_inverse_f32.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_inverse_f64.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_mult_f32.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_mult_fast_q15.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_mult_fast_q31.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_mult_q15.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_mult_q31.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_scale_f32.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_scale_q15.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_scale_q31.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_sub_f32.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_sub_q15.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_sub_q31.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_trans_f32.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_trans_q15.d \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_trans_q31.d 

OBJS += \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_add_f32.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_add_q15.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_add_q31.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_cmplx_mult_f32.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_cmplx_mult_q15.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_cmplx_mult_q31.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_init_f32.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_init_q15.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_init_q31.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_inverse_f32.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_inverse_f64.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_mult_f32.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_mult_fast_q15.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_mult_fast_q31.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_mult_q15.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_mult_q31.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_scale_f32.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_scale_q15.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_scale_q31.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_sub_f32.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_sub_q15.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_sub_q31.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_trans_f32.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_trans_q15.obj \
./CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_trans_q31.obj 

OBJS__QUOTED += \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\MatrixFunctions\arm_mat_add_f32.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\MatrixFunctions\arm_mat_add_q15.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\MatrixFunctions\arm_mat_add_q31.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\MatrixFunctions\arm_mat_cmplx_mult_f32.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\MatrixFunctions\arm_mat_cmplx_mult_q15.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\MatrixFunctions\arm_mat_cmplx_mult_q31.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\MatrixFunctions\arm_mat_init_f32.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\MatrixFunctions\arm_mat_init_q15.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\MatrixFunctions\arm_mat_init_q31.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\MatrixFunctions\arm_mat_inverse_f32.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\MatrixFunctions\arm_mat_inverse_f64.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\MatrixFunctions\arm_mat_mult_f32.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\MatrixFunctions\arm_mat_mult_fast_q15.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\MatrixFunctions\arm_mat_mult_fast_q31.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\MatrixFunctions\arm_mat_mult_q15.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\MatrixFunctions\arm_mat_mult_q31.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\MatrixFunctions\arm_mat_scale_f32.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\MatrixFunctions\arm_mat_scale_q15.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\MatrixFunctions\arm_mat_scale_q31.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\MatrixFunctions\arm_mat_sub_f32.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\MatrixFunctions\arm_mat_sub_q15.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\MatrixFunctions\arm_mat_sub_q31.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\MatrixFunctions\arm_mat_trans_f32.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\MatrixFunctions\arm_mat_trans_q15.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\MatrixFunctions\arm_mat_trans_q31.obj" 

C_DEPS__QUOTED += \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\MatrixFunctions\arm_mat_add_f32.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\MatrixFunctions\arm_mat_add_q15.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\MatrixFunctions\arm_mat_add_q31.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\MatrixFunctions\arm_mat_cmplx_mult_f32.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\MatrixFunctions\arm_mat_cmplx_mult_q15.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\MatrixFunctions\arm_mat_cmplx_mult_q31.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\MatrixFunctions\arm_mat_init_f32.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\MatrixFunctions\arm_mat_init_q15.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\MatrixFunctions\arm_mat_init_q31.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\MatrixFunctions\arm_mat_inverse_f32.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\MatrixFunctions\arm_mat_inverse_f64.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\MatrixFunctions\arm_mat_mult_f32.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\MatrixFunctions\arm_mat_mult_fast_q15.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\MatrixFunctions\arm_mat_mult_fast_q31.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\MatrixFunctions\arm_mat_mult_q15.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\MatrixFunctions\arm_mat_mult_q31.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\MatrixFunctions\arm_mat_scale_f32.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\MatrixFunctions\arm_mat_scale_q15.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\MatrixFunctions\arm_mat_scale_q31.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\MatrixFunctions\arm_mat_sub_f32.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\MatrixFunctions\arm_mat_sub_q15.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\MatrixFunctions\arm_mat_sub_q31.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\MatrixFunctions\arm_mat_trans_f32.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\MatrixFunctions\arm_mat_trans_q15.d" \
"CMSIS-master\CMSIS-master\CMSIS\DSP_Lib\Source\MatrixFunctions\arm_mat_trans_q31.d" 

C_SRCS__QUOTED += \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_add_f32.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_add_q15.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_add_q31.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_cmplx_mult_f32.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_cmplx_mult_q15.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_cmplx_mult_q31.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_init_f32.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_init_q15.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_init_q31.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_inverse_f32.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_inverse_f64.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_mult_f32.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_mult_fast_q15.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_mult_fast_q31.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_mult_q15.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_mult_q31.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_scale_f32.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_scale_q15.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_scale_q31.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_sub_f32.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_sub_q15.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_sub_q31.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_trans_f32.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_trans_q15.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_trans_q31.c" 


