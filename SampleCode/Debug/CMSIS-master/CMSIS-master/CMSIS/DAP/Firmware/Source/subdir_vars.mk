################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../CMSIS-master/CMSIS-master/CMSIS/DAP/Firmware/Source/DAP.c \
../CMSIS-master/CMSIS-master/CMSIS/DAP/Firmware/Source/DAP_vendor.c \
../CMSIS-master/CMSIS-master/CMSIS/DAP/Firmware/Source/JTAG_DP.c \
../CMSIS-master/CMSIS-master/CMSIS/DAP/Firmware/Source/SWO.c \
../CMSIS-master/CMSIS-master/CMSIS/DAP/Firmware/Source/SW_DP.c 

C_DEPS += \
./CMSIS-master/CMSIS-master/CMSIS/DAP/Firmware/Source/DAP.d \
./CMSIS-master/CMSIS-master/CMSIS/DAP/Firmware/Source/DAP_vendor.d \
./CMSIS-master/CMSIS-master/CMSIS/DAP/Firmware/Source/JTAG_DP.d \
./CMSIS-master/CMSIS-master/CMSIS/DAP/Firmware/Source/SWO.d \
./CMSIS-master/CMSIS-master/CMSIS/DAP/Firmware/Source/SW_DP.d 

OBJS += \
./CMSIS-master/CMSIS-master/CMSIS/DAP/Firmware/Source/DAP.obj \
./CMSIS-master/CMSIS-master/CMSIS/DAP/Firmware/Source/DAP_vendor.obj \
./CMSIS-master/CMSIS-master/CMSIS/DAP/Firmware/Source/JTAG_DP.obj \
./CMSIS-master/CMSIS-master/CMSIS/DAP/Firmware/Source/SWO.obj \
./CMSIS-master/CMSIS-master/CMSIS/DAP/Firmware/Source/SW_DP.obj 

OBJS__QUOTED += \
"CMSIS-master\CMSIS-master\CMSIS\DAP\Firmware\Source\DAP.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DAP\Firmware\Source\DAP_vendor.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DAP\Firmware\Source\JTAG_DP.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DAP\Firmware\Source\SWO.obj" \
"CMSIS-master\CMSIS-master\CMSIS\DAP\Firmware\Source\SW_DP.obj" 

C_DEPS__QUOTED += \
"CMSIS-master\CMSIS-master\CMSIS\DAP\Firmware\Source\DAP.d" \
"CMSIS-master\CMSIS-master\CMSIS\DAP\Firmware\Source\DAP_vendor.d" \
"CMSIS-master\CMSIS-master\CMSIS\DAP\Firmware\Source\JTAG_DP.d" \
"CMSIS-master\CMSIS-master\CMSIS\DAP\Firmware\Source\SWO.d" \
"CMSIS-master\CMSIS-master\CMSIS\DAP\Firmware\Source\SW_DP.d" 

C_SRCS__QUOTED += \
"../CMSIS-master/CMSIS-master/CMSIS/DAP/Firmware/Source/DAP.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DAP/Firmware/Source/DAP_vendor.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DAP/Firmware/Source/JTAG_DP.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DAP/Firmware/Source/SWO.c" \
"../CMSIS-master/CMSIS-master/CMSIS/DAP/Firmware/Source/SW_DP.c" 


