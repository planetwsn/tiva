################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/HAL_CM.c \
../CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_CMSIS.c \
../CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_Event.c \
../CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_List.c \
../CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_Mailbox.c \
../CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_MemBox.c \
../CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_Memory.c \
../CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_Mutex.c \
../CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_Robin.c \
../CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_Semaphore.c \
../CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_System.c \
../CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_Task.c \
../CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_Time.c \
../CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_Timer.c 

C_DEPS += \
./CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/HAL_CM.d \
./CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_CMSIS.d \
./CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_Event.d \
./CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_List.d \
./CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_Mailbox.d \
./CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_MemBox.d \
./CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_Memory.d \
./CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_Mutex.d \
./CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_Robin.d \
./CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_Semaphore.d \
./CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_System.d \
./CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_Task.d \
./CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_Time.d \
./CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_Timer.d 

OBJS += \
./CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/HAL_CM.obj \
./CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_CMSIS.obj \
./CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_Event.obj \
./CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_List.obj \
./CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_Mailbox.obj \
./CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_MemBox.obj \
./CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_Memory.obj \
./CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_Mutex.obj \
./CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_Robin.obj \
./CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_Semaphore.obj \
./CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_System.obj \
./CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_Task.obj \
./CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_Time.obj \
./CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_Timer.obj 

OBJS__QUOTED += \
"CMSIS-master\CMSIS-master\CMSIS\RTOS\RTX\SRC\HAL_CM.obj" \
"CMSIS-master\CMSIS-master\CMSIS\RTOS\RTX\SRC\rt_CMSIS.obj" \
"CMSIS-master\CMSIS-master\CMSIS\RTOS\RTX\SRC\rt_Event.obj" \
"CMSIS-master\CMSIS-master\CMSIS\RTOS\RTX\SRC\rt_List.obj" \
"CMSIS-master\CMSIS-master\CMSIS\RTOS\RTX\SRC\rt_Mailbox.obj" \
"CMSIS-master\CMSIS-master\CMSIS\RTOS\RTX\SRC\rt_MemBox.obj" \
"CMSIS-master\CMSIS-master\CMSIS\RTOS\RTX\SRC\rt_Memory.obj" \
"CMSIS-master\CMSIS-master\CMSIS\RTOS\RTX\SRC\rt_Mutex.obj" \
"CMSIS-master\CMSIS-master\CMSIS\RTOS\RTX\SRC\rt_Robin.obj" \
"CMSIS-master\CMSIS-master\CMSIS\RTOS\RTX\SRC\rt_Semaphore.obj" \
"CMSIS-master\CMSIS-master\CMSIS\RTOS\RTX\SRC\rt_System.obj" \
"CMSIS-master\CMSIS-master\CMSIS\RTOS\RTX\SRC\rt_Task.obj" \
"CMSIS-master\CMSIS-master\CMSIS\RTOS\RTX\SRC\rt_Time.obj" \
"CMSIS-master\CMSIS-master\CMSIS\RTOS\RTX\SRC\rt_Timer.obj" 

C_DEPS__QUOTED += \
"CMSIS-master\CMSIS-master\CMSIS\RTOS\RTX\SRC\HAL_CM.d" \
"CMSIS-master\CMSIS-master\CMSIS\RTOS\RTX\SRC\rt_CMSIS.d" \
"CMSIS-master\CMSIS-master\CMSIS\RTOS\RTX\SRC\rt_Event.d" \
"CMSIS-master\CMSIS-master\CMSIS\RTOS\RTX\SRC\rt_List.d" \
"CMSIS-master\CMSIS-master\CMSIS\RTOS\RTX\SRC\rt_Mailbox.d" \
"CMSIS-master\CMSIS-master\CMSIS\RTOS\RTX\SRC\rt_MemBox.d" \
"CMSIS-master\CMSIS-master\CMSIS\RTOS\RTX\SRC\rt_Memory.d" \
"CMSIS-master\CMSIS-master\CMSIS\RTOS\RTX\SRC\rt_Mutex.d" \
"CMSIS-master\CMSIS-master\CMSIS\RTOS\RTX\SRC\rt_Robin.d" \
"CMSIS-master\CMSIS-master\CMSIS\RTOS\RTX\SRC\rt_Semaphore.d" \
"CMSIS-master\CMSIS-master\CMSIS\RTOS\RTX\SRC\rt_System.d" \
"CMSIS-master\CMSIS-master\CMSIS\RTOS\RTX\SRC\rt_Task.d" \
"CMSIS-master\CMSIS-master\CMSIS\RTOS\RTX\SRC\rt_Time.d" \
"CMSIS-master\CMSIS-master\CMSIS\RTOS\RTX\SRC\rt_Timer.d" 

C_SRCS__QUOTED += \
"../CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/HAL_CM.c" \
"../CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_CMSIS.c" \
"../CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_Event.c" \
"../CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_List.c" \
"../CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_Mailbox.c" \
"../CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_MemBox.c" \
"../CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_Memory.c" \
"../CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_Mutex.c" \
"../CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_Robin.c" \
"../CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_Semaphore.c" \
"../CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_System.c" \
"../CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_Task.c" \
"../CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_Time.c" \
"../CMSIS-master/CMSIS-master/CMSIS/RTOS/RTX/SRC/rt_Timer.c" 


