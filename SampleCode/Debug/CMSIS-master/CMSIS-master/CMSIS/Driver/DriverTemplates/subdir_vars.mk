################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_CAN.c \
../CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_ETH_MAC.c \
../CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_ETH_PHY.c \
../CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_Flash.c \
../CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_I2C.c \
../CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_MCI.c \
../CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_SAI.c \
../CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_SPI.c \
../CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_USART.c \
../CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_USBD.c \
../CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_USBH.c 

C_DEPS += \
./CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_CAN.d \
./CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_ETH_MAC.d \
./CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_ETH_PHY.d \
./CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_Flash.d \
./CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_I2C.d \
./CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_MCI.d \
./CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_SAI.d \
./CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_SPI.d \
./CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_USART.d \
./CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_USBD.d \
./CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_USBH.d 

OBJS += \
./CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_CAN.obj \
./CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_ETH_MAC.obj \
./CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_ETH_PHY.obj \
./CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_Flash.obj \
./CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_I2C.obj \
./CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_MCI.obj \
./CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_SAI.obj \
./CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_SPI.obj \
./CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_USART.obj \
./CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_USBD.obj \
./CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_USBH.obj 

OBJS__QUOTED += \
"CMSIS-master\CMSIS-master\CMSIS\Driver\DriverTemplates\Driver_CAN.obj" \
"CMSIS-master\CMSIS-master\CMSIS\Driver\DriverTemplates\Driver_ETH_MAC.obj" \
"CMSIS-master\CMSIS-master\CMSIS\Driver\DriverTemplates\Driver_ETH_PHY.obj" \
"CMSIS-master\CMSIS-master\CMSIS\Driver\DriverTemplates\Driver_Flash.obj" \
"CMSIS-master\CMSIS-master\CMSIS\Driver\DriverTemplates\Driver_I2C.obj" \
"CMSIS-master\CMSIS-master\CMSIS\Driver\DriverTemplates\Driver_MCI.obj" \
"CMSIS-master\CMSIS-master\CMSIS\Driver\DriverTemplates\Driver_SAI.obj" \
"CMSIS-master\CMSIS-master\CMSIS\Driver\DriverTemplates\Driver_SPI.obj" \
"CMSIS-master\CMSIS-master\CMSIS\Driver\DriverTemplates\Driver_USART.obj" \
"CMSIS-master\CMSIS-master\CMSIS\Driver\DriverTemplates\Driver_USBD.obj" \
"CMSIS-master\CMSIS-master\CMSIS\Driver\DriverTemplates\Driver_USBH.obj" 

C_DEPS__QUOTED += \
"CMSIS-master\CMSIS-master\CMSIS\Driver\DriverTemplates\Driver_CAN.d" \
"CMSIS-master\CMSIS-master\CMSIS\Driver\DriverTemplates\Driver_ETH_MAC.d" \
"CMSIS-master\CMSIS-master\CMSIS\Driver\DriverTemplates\Driver_ETH_PHY.d" \
"CMSIS-master\CMSIS-master\CMSIS\Driver\DriverTemplates\Driver_Flash.d" \
"CMSIS-master\CMSIS-master\CMSIS\Driver\DriverTemplates\Driver_I2C.d" \
"CMSIS-master\CMSIS-master\CMSIS\Driver\DriverTemplates\Driver_MCI.d" \
"CMSIS-master\CMSIS-master\CMSIS\Driver\DriverTemplates\Driver_SAI.d" \
"CMSIS-master\CMSIS-master\CMSIS\Driver\DriverTemplates\Driver_SPI.d" \
"CMSIS-master\CMSIS-master\CMSIS\Driver\DriverTemplates\Driver_USART.d" \
"CMSIS-master\CMSIS-master\CMSIS\Driver\DriverTemplates\Driver_USBD.d" \
"CMSIS-master\CMSIS-master\CMSIS\Driver\DriverTemplates\Driver_USBH.d" 

C_SRCS__QUOTED += \
"../CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_CAN.c" \
"../CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_ETH_MAC.c" \
"../CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_ETH_PHY.c" \
"../CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_Flash.c" \
"../CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_I2C.c" \
"../CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_MCI.c" \
"../CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_SAI.c" \
"../CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_SPI.c" \
"../CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_USART.c" \
"../CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_USBD.c" \
"../CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_USBH.c" 


