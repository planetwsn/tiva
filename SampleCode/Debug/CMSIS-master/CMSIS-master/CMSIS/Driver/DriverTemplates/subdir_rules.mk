################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Each subdirectory must supply rules for building sources it contributes
CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_CAN.obj: ../CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_CAN.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_CAN.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_ETH_MAC.obj: ../CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_ETH_MAC.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_ETH_MAC.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_ETH_PHY.obj: ../CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_ETH_PHY.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_ETH_PHY.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_Flash.obj: ../CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_Flash.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_Flash.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_I2C.obj: ../CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_I2C.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_I2C.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_MCI.obj: ../CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_MCI.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_MCI.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_SAI.obj: ../CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_SAI.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_SAI.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_SPI.obj: ../CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_SPI.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_SPI.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_USART.obj: ../CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_USART.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_USART.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_USBD.obj: ../CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_USBD.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_USBD.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_USBH.obj: ../CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_USBH.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/macab/Documents/CCS/TivaTemplate/SampleCode" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --define=ccs="ccs" --define=PART_TM4C123GH6PM -g --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates/Driver_USBH.d_raw" --obj_directory="CMSIS-master/CMSIS-master/CMSIS/Driver/DriverTemplates" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '


