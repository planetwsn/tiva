################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/EMAC_LPC18xx.c \
../CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/GPDMA_LPC18xx.c \
../CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/GPIO_LPC18xx.c \
../CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/I2C_LPC18xx.c \
../CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/I2S_LPC18xx.c \
../CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/MCI_LPC18xx.c \
../CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/SCU_LPC18xx.c \
../CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/SSP_LPC18xx.c \
../CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/USART_LPC18xx.c \
../CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/USB0_LPC18xx.c \
../CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/USB1_LPC18xx.c \
../CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/USBD0_LPC18xx.c \
../CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/USBD1_LPC18xx.c \
../CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/USBH0_LPC18xx.c \
../CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/USBH1_LPC18xx.c 

C_DEPS += \
./CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/EMAC_LPC18xx.d \
./CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/GPDMA_LPC18xx.d \
./CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/GPIO_LPC18xx.d \
./CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/I2C_LPC18xx.d \
./CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/I2S_LPC18xx.d \
./CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/MCI_LPC18xx.d \
./CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/SCU_LPC18xx.d \
./CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/SSP_LPC18xx.d \
./CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/USART_LPC18xx.d \
./CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/USB0_LPC18xx.d \
./CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/USB1_LPC18xx.d \
./CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/USBD0_LPC18xx.d \
./CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/USBD1_LPC18xx.d \
./CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/USBH0_LPC18xx.d \
./CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/USBH1_LPC18xx.d 

OBJS += \
./CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/EMAC_LPC18xx.obj \
./CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/GPDMA_LPC18xx.obj \
./CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/GPIO_LPC18xx.obj \
./CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/I2C_LPC18xx.obj \
./CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/I2S_LPC18xx.obj \
./CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/MCI_LPC18xx.obj \
./CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/SCU_LPC18xx.obj \
./CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/SSP_LPC18xx.obj \
./CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/USART_LPC18xx.obj \
./CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/USB0_LPC18xx.obj \
./CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/USB1_LPC18xx.obj \
./CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/USBD0_LPC18xx.obj \
./CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/USBD1_LPC18xx.obj \
./CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/USBH0_LPC18xx.obj \
./CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/USBH1_LPC18xx.obj 

OBJS__QUOTED += \
"CMSIS-master\CMSIS-master\CMSIS\Pack\Example\CMSIS_Driver\EMAC_LPC18xx.obj" \
"CMSIS-master\CMSIS-master\CMSIS\Pack\Example\CMSIS_Driver\GPDMA_LPC18xx.obj" \
"CMSIS-master\CMSIS-master\CMSIS\Pack\Example\CMSIS_Driver\GPIO_LPC18xx.obj" \
"CMSIS-master\CMSIS-master\CMSIS\Pack\Example\CMSIS_Driver\I2C_LPC18xx.obj" \
"CMSIS-master\CMSIS-master\CMSIS\Pack\Example\CMSIS_Driver\I2S_LPC18xx.obj" \
"CMSIS-master\CMSIS-master\CMSIS\Pack\Example\CMSIS_Driver\MCI_LPC18xx.obj" \
"CMSIS-master\CMSIS-master\CMSIS\Pack\Example\CMSIS_Driver\SCU_LPC18xx.obj" \
"CMSIS-master\CMSIS-master\CMSIS\Pack\Example\CMSIS_Driver\SSP_LPC18xx.obj" \
"CMSIS-master\CMSIS-master\CMSIS\Pack\Example\CMSIS_Driver\USART_LPC18xx.obj" \
"CMSIS-master\CMSIS-master\CMSIS\Pack\Example\CMSIS_Driver\USB0_LPC18xx.obj" \
"CMSIS-master\CMSIS-master\CMSIS\Pack\Example\CMSIS_Driver\USB1_LPC18xx.obj" \
"CMSIS-master\CMSIS-master\CMSIS\Pack\Example\CMSIS_Driver\USBD0_LPC18xx.obj" \
"CMSIS-master\CMSIS-master\CMSIS\Pack\Example\CMSIS_Driver\USBD1_LPC18xx.obj" \
"CMSIS-master\CMSIS-master\CMSIS\Pack\Example\CMSIS_Driver\USBH0_LPC18xx.obj" \
"CMSIS-master\CMSIS-master\CMSIS\Pack\Example\CMSIS_Driver\USBH1_LPC18xx.obj" 

C_DEPS__QUOTED += \
"CMSIS-master\CMSIS-master\CMSIS\Pack\Example\CMSIS_Driver\EMAC_LPC18xx.d" \
"CMSIS-master\CMSIS-master\CMSIS\Pack\Example\CMSIS_Driver\GPDMA_LPC18xx.d" \
"CMSIS-master\CMSIS-master\CMSIS\Pack\Example\CMSIS_Driver\GPIO_LPC18xx.d" \
"CMSIS-master\CMSIS-master\CMSIS\Pack\Example\CMSIS_Driver\I2C_LPC18xx.d" \
"CMSIS-master\CMSIS-master\CMSIS\Pack\Example\CMSIS_Driver\I2S_LPC18xx.d" \
"CMSIS-master\CMSIS-master\CMSIS\Pack\Example\CMSIS_Driver\MCI_LPC18xx.d" \
"CMSIS-master\CMSIS-master\CMSIS\Pack\Example\CMSIS_Driver\SCU_LPC18xx.d" \
"CMSIS-master\CMSIS-master\CMSIS\Pack\Example\CMSIS_Driver\SSP_LPC18xx.d" \
"CMSIS-master\CMSIS-master\CMSIS\Pack\Example\CMSIS_Driver\USART_LPC18xx.d" \
"CMSIS-master\CMSIS-master\CMSIS\Pack\Example\CMSIS_Driver\USB0_LPC18xx.d" \
"CMSIS-master\CMSIS-master\CMSIS\Pack\Example\CMSIS_Driver\USB1_LPC18xx.d" \
"CMSIS-master\CMSIS-master\CMSIS\Pack\Example\CMSIS_Driver\USBD0_LPC18xx.d" \
"CMSIS-master\CMSIS-master\CMSIS\Pack\Example\CMSIS_Driver\USBD1_LPC18xx.d" \
"CMSIS-master\CMSIS-master\CMSIS\Pack\Example\CMSIS_Driver\USBH0_LPC18xx.d" \
"CMSIS-master\CMSIS-master\CMSIS\Pack\Example\CMSIS_Driver\USBH1_LPC18xx.d" 

C_SRCS__QUOTED += \
"../CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/EMAC_LPC18xx.c" \
"../CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/GPDMA_LPC18xx.c" \
"../CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/GPIO_LPC18xx.c" \
"../CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/I2C_LPC18xx.c" \
"../CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/I2S_LPC18xx.c" \
"../CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/MCI_LPC18xx.c" \
"../CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/SCU_LPC18xx.c" \
"../CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/SSP_LPC18xx.c" \
"../CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/USART_LPC18xx.c" \
"../CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/USB0_LPC18xx.c" \
"../CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/USB1_LPC18xx.c" \
"../CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/USBD0_LPC18xx.c" \
"../CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/USBD1_LPC18xx.c" \
"../CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/USBH0_LPC18xx.c" \
"../CMSIS-master/CMSIS-master/CMSIS/Pack/Example/CMSIS_Driver/USBH1_LPC18xx.c" 


