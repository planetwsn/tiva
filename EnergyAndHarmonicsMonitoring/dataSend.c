/*
*/

#include <stdio.h>
#include "initboard.h"
#include "dataSend.h"

void transmitData(tdData_ptr td, fdData_ptr fd){
    char txStr[53];
    int index;

    sprintf(txStr, "%06.2f&%05.2f&%07.2f&%07.2f&"
            "%07.2f&%05.2f&%06.2f#",
            td->vrms,td->irms,td->ppwr + 2500,td->qpwr + 2500,
            td->spwr + 2500,td->freq,td->ph + 360);

    for(index = 0; index < 52; index++){
        UARTCharPut(UART0_BASE, txStr[index]);
//        UARTCharPut(UART1_BASE, txStr[index]);
    }
}
