/* Header file for time domain analysis functions
		Written by:
		Date:
*/
#ifndef TDA_H_
#define TDA_H_

#include "datastruct.h"
#include "initboard.h"

#define __BSD_VISIBLE // For selecting math functions from math library
#include <math.h>

void SampleWaveform (dtWaveform_ptr reading);
void signalCorrection(dtWaveform_ptr reading);
void vectorization (dtWaveform_ptr reading, zcData_ptr zc);
void tdCompute (dtWaveform_ptr reading, zcData_ptr zc, tdData_ptr output);

#endif // TDA_H_
