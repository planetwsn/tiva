/*  Main program for the energy monitoring module program
 * for Tiva C Series Launchpad (TM4C123G)
 *      Written by:
 *      Date:
 */
#include "initboard.h"
#include "tda.h"
#include "fda.h"
#include "dataSend.h"

dtWaveform_t dtSignals;
zcData_t zc;
tdData_t tdOutput;
//        fdData_t fdOutput;
/**
 * main.c
 */
void main (void)
{

// Statements
    InitBoard();

//    { // Power and harmonics Analyzer
    // Local Declarations


    //Statements
        while(1){
               SampleWaveform(&dtSignals);
               signalCorrection(&dtSignals);
               vectorization(&dtSignals, &zc);
               tdCompute(&dtSignals, &zc, &tdOutput);
 //              fdCompute(&dtSignals, &zc, &fdOutput);
 //              transmitData(&tdOutput, &fdOutput);
       } // while
//    } // Power and harmonics analyzer
}
