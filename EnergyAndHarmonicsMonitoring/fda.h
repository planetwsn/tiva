/* 
		Written by:
		Date:
*/
#ifndef FDA_H_
#define FDA_H_

#include "datastruct.h"
#define __BSD_VISIBLE // For selecting math functions from math library
#include <math.h>
#include "fftCompute.h"

void fdCompute(dtWaveform_ptr reading,
			   zcData_ptr zc, 
			   fdData_ptr output);

#endif	//  FDA_H_

