/*
*/
#include "fda.h"


void _fd_pwr(int32_t numharms, harmonicData_ptr vF, harmonicData_ptr iF, fdData_ptr pdata)
{
    int32_t npoints = numharms, ix;
    float t_vrms        = 0;
    float t_irms        = 0;

    //vrms of fundamental frequency component
    t_vrms          = ((*(vF + 1)).r * (*(vF + 1)).r + (*(vF + 1)).i * (*(vF + 1)).i);
    t_irms          = ((*(iF + 1)).r * (*(iF + 1)).r + (*(iF + 1)).i * (*(iF + 1)).i);

    pdata->vrms1         = sqrtf( t_vrms ) / M_SQRT2;
    pdata->irms1         = sqrtf( t_irms ) / M_SQRT2;

    //Harmonics vrms sum
    t_vrms = 0;
    t_irms = 0;
    for (ix = 2; ix < npoints; ix++)
    {
        t_vrms          += (*(vF + ix)).r * (*(vF + ix)).r + (*(vF + ix)).i * (*(vF + ix)).i;
        t_irms          += (*(iF + ix)).r * (*(iF + ix)).r + (*(iF + ix)).i * (*(iF + ix)).i;
    }
    pdata->vrmsh      = sqrtf( t_vrms ) / 8.0;
    pdata->irmsh      = sqrtf( t_irms ) / 8.0;
    pdata->thd        = pdata->irmsh / pdata->irms1;
    return;
}

void fdCompute(dtWaveform_ptr raw, zcData_ptr zc, fdData_ptr output){

    float vreal[256], ireal[256], vimag[256], iimag[256];
    harmonicData_t v_harm[10], i_harm[10];

    uint16_t n = zc->vwIntg[0], m = zc->iwIntg[0], i;

    for (i = 0; i < 256; i++){
        vreal[i] = raw->voltage[n + i];
        vimag[i] = 0;
        ireal[i] = raw->current[m + i];
        iimag[i] = 0;
    }

    fft(vreal, vimag, 256);
    fft(ireal, iimag, 256);

    v_harm[0].r = vreal[4];
    v_harm[0].i = vimag[4];
    v_harm[0].r = ireal[4];
    i_harm[0].i = iimag[4];

    n = 4;
    //fundamental and 18 harmonics
    for(i = 1; i < 10; i++){
        v_harm[i].r = vreal[n];
        v_harm[i].i = vimag[n];
        i_harm[i].r = ireal[n];
        i_harm[i].i = iimag[n];
        n += 8;
    }
    // just for plotting
    for(i = 0; i < 256; i++){
        vreal[i] = sqrtf(vreal[i]*vreal[i] + vimag[i]*vimag[i]);
        ireal[i] = sqrtf(ireal[i]*ireal[i] + iimag[i]*iimag[i]);
    }
    _fd_pwr(10, v_harm, i_harm, output);
    return;
}
