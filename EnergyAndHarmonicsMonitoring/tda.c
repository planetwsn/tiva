/* Computes for the Time Domain parameters
		Written by:
		Date:
*/
#include "tda.h"

/* ================= sampleWaveform ==================
	Samples a CT waveforms of voltage and current;
	stores DT signal in two separate arrays
	
		Pre		New reading needed
		Post	Returns a DT signal in dtWaveform 
				structure
*/
void SampleWaveform (dtWaveform_ptr reading)
{
// Local Declaration
    uint32_t rawVoltageBuffer[4], rawCurrentBuffer[4];
    int index;

// Statements
	// Enable sampling timer
    index = -1;
	_enableSampling();

	// While DT signal arrays are not yet filled
	while(1){
	    if(isSampleReady){
            // Retrieve sample for voltage
            ADCIntClear(ADC0_BASE, SAMPLE_SEQUENCER1);
            ADCSequenceDataGet(ADC0_BASE, SAMPLE_SEQUENCER1,
                               rawVoltageBuffer);
            if(index >= 0)
                reading->voltage[index] = (float)((rawVoltageBuffer[0] & 0xFFF)
                    + (rawVoltageBuffer[1] & 0xFFF)
                    + (rawVoltageBuffer[2] & 0xFFF)
                    + (rawVoltageBuffer[3] & 0xFFF))/4.0;

            // Retrieve sample for current
            ADCIntClear(ADC1_BASE, SAMPLE_SEQUENCER1);
            ADCSequenceDataGet(ADC1_BASE, SAMPLE_SEQUENCER1,
                               rawCurrentBuffer);
            if(index >= 0)
                reading->current[index] = (float)((rawCurrentBuffer[0] & 0xFFF)
                    + (rawCurrentBuffer[1] & 0xFFF)
                    + (rawCurrentBuffer[2] & 0xFFF)
                    + (rawCurrentBuffer[3] & 0xFFF))/4.0;

            isSampleReady = false;

            // Check if arrays are filled
            index += 1;
            if (index == SAMPLESIZE)
                break;
	    }// if sample ready
	}
	
	// Disable sampling timer
	_disableSampling();
	return;
}

/* ================ signalCorrection ================
	Corrects gain and offset errors
	
		pre		sampled DT signals
		post	DT signal equivalent of original 
				CT signals
*/
void signalCorrection(dtWaveform_ptr raw)
{
    float v_ave = 0.0, i_ave = 0.0;
    uint16_t index;

    // Amplify signals to maximise filtering of noise
    for (index = 0; index < SAMPLESIZE; index++){
        raw->voltage[index] *= CORRECTING_GAIN;
        raw->current[index] *= CORRECTING_GAIN;
    }

    // Compute the DC component by solving the signal average
    // using the moving average algorithm
    for (index = 0; index < SAMPLESIZE; index++){
        v_ave = (v_ave*index + raw->voltage[index])/(index+1);
        i_ave = (i_ave*index + raw->current[index])/(index+1);
    }

    // Correct signals
    for (index = 0; index < SAMPLESIZE; index++){
        raw->voltage[index] -= v_ave;
        raw->current[index] -= i_ave;
    }

    //second averaging
    for (index = 0; index < SAMPLESIZE; index++){
        v_ave = (v_ave*index + raw->voltage[index])/(index+1);
        i_ave = (i_ave*index + raw->current[index])/(index+1);
    }
    // Correct signals
    for (index = 0; index < SAMPLESIZE; index++){
        raw->voltage[index] = (raw->voltage[index] - v_ave) * VOLTAGE_CONDITIONING_FACTOR;
        raw->current[index] = (raw->current[index] - i_ave) * CURRENT_CONDITIONING_FACTOR;
    }

    // Applying calibration values
    for (index = 0; index < SAMPLESIZE; index++){
        raw->voltage[index] = (raw->voltage[index] - VOLTAGE_OFFSET_CALIBRATION)/VOLTAGE_GAIN_CALIBRATION;
        raw->current[index] = (raw->current[index] - CURRENT_OFFSET_CALIBRATION)/CURRENT_GAIN_CALIBRATION;
    }
	return;
}

/* ================== vectorization =================
	Locates zero-crossing points in the DT signals
		
		Pre 	conditioned DT signals
		Post 	indices of located zero-cross points
*/
void vectorization (dtWaveform_ptr reading, 
					zcData_ptr zc)
{
// Local Declarations
    uint16_t signalIndex;
    uint8_t zcCount;

// Statements
    // Initialise all zc values to be zero
    for(zcCount = 0; zcCount < 4; zcCount++){
        zc->vwIntg[zcCount] = 0;
        zc->vwFrac[zcCount] = 0;
        zc->iwIntg[zcCount] = 0;
        zc->iwFrac[zcCount] = 0;
    }

	// Locate positive-going zero-crossing points
	// for the voltage DT waveform
    for (signalIndex = 0, zcCount = 0; signalIndex < SAMPLESIZE - 1; signalIndex++){
        if((reading->voltage[signalIndex] <= 0) && (reading->voltage[signalIndex + 1] > 0)){
            zc->vwIntg[zcCount] = signalIndex;
            zc->vwFrac[zcCount] = 4095 * -(reading->voltage[signalIndex])
                    /(reading->voltage[signalIndex + 1]-reading->voltage[signalIndex]);

            zcCount++;
            if(zcCount == 4)
                break;
        } // if
    } // for

    zc->vwIntg[0] += 1;
    zc->vwFrac[0] = 4095 - zc->vwFrac[0];

	// Locate positive-going zero-crossing points
	// for the current DT waveform
    for (signalIndex = 0, zcCount = 0; signalIndex < SAMPLESIZE - 1; signalIndex++){
        if((reading->current[signalIndex] <= 0) && (reading->current[signalIndex + 1] > 0)){
            zc->iwIntg[zcCount] = signalIndex;
            zc->iwFrac[zcCount] = 4095 * -(reading->current[signalIndex])
                    /(reading->current[signalIndex + 1]-reading->current[signalIndex]);

            zcCount++;
            if(zcCount == 4)
                break;
        } // if
    } // for

    zc->iwIntg[0] += 1;
    zc->iwFrac[0] = 4095 - zc->iwFrac[0];

	return;
}

/* ================= TD Compute =====================
	Computes for the electrical parameters from the
	given DT signals
	
		Pre 	conditioned TD signals with zero-
				cross indices
		Post	electrical parameters values
*/
void tdCompute (dtWaveform_ptr reading, zcData_ptr zc,
				tdData_ptr output)
{
// Local Declaration
    float periodpts = 0, vwSurplus, pwSurplus, iwSurplus,
            vwSquare, iwSquare, pwSquare,
            edgeV, edgeI, edgeP;
    uint16_t i;
    uint32_t vwEndIndex, iwEndIndex,
            vwStartIndex, iwStartIndex;

// Statements
	// Compute period
	periodpts = ((float)(zc->vwIntg[1])
	        + (float)(zc->vwFrac[1])/4095.0)
	        - ((float)(zc->vwIntg[0])
	        + (float)(zc->vwFrac[0])/4095.0);
	vwStartIndex = zc->vwIntg[0];
	iwStartIndex = zc->iwIntg[0];

	// Integrate voltage, current, and power waveforms
	if ((zc->vwIntg[1] - zc->vwIntg[0]) % 2 == 1){
	    vwSurplus = 0.5 * (float)(reading->voltage[zc->vwIntg[1]]
	                * reading->voltage[zc->vwIntg[1]]
	                + reading->voltage[zc->vwIntg[0]]
	                * reading->voltage[zc->vwIntg[0]]);
	    pwSurplus = 0.5 * (float)(reading->voltage[zc->vwIntg[1]]
	                * reading->current[zc->iwIntg[1]]
	                + reading->voltage[zc->vwIntg[0]]
	                * reading->current[zc->iwIntg[0]]);
	    vwEndIndex = zc->vwIntg[1] - 1;
	} else {
	    vwSurplus = 0;
	    vwEndIndex = zc->vwIntg[1];
	    pwSurplus = 0;
	} // if (voltage parity)

	if ((zc->iwIntg[1] - zc->iwIntg[0]) % 2 == 1){
	    iwSurplus = 0.5 * (float)(reading->current[zc->iwIntg[1]]
	                * reading->current[zc->iwIntg[1]]
	                + reading->current[zc->iwIntg[0]]
	                * reading->current[zc->iwIntg[0]]);
	    iwEndIndex = zc->iwIntg[1] - 1;
	} else{
	    iwSurplus = 0;
	    iwEndIndex = zc->iwIntg[1];
	} // if (current parity)

    vwSquare = (float)((reading->voltage[vwStartIndex]
              * reading->voltage[vwStartIndex])
              + (reading->voltage[vwEndIndex]
              * reading->voltage[vwEndIndex]))
              + vwSurplus;
    iwSquare = (float)((reading->current[iwStartIndex]
              * reading->current[iwStartIndex])
              + (reading->current[iwEndIndex]
              * reading->current[iwEndIndex]))
              + iwSurplus;
    pwSquare = (float)((reading->voltage[vwStartIndex]
              * reading->current[iwStartIndex])
              + (reading->voltage[vwEndIndex]
              * reading->current[iwEndIndex]))
              + pwSurplus;

    for (i = vwStartIndex + 1; i <= vwEndIndex - 1;
            i += 2){
        vwSquare += (float)(4 * (reading->voltage[i]
                      * reading->voltage[i])
                  + 2 * (reading->voltage[i+1]
                      * reading->voltage[i+1]));
        pwSquare += (float)(4 * (reading->voltage[i]
                      * reading->current[i])
                  + 2 * (reading->voltage[i+1]
                      * reading->current[i+1]));
    }

    for (i = iwStartIndex + 1; i <= iwEndIndex - 1;
            i += 2){
        iwSquare += (float)(4 * (reading->current[i]
                      * reading->current[i])
                  + 2 * (reading->current[i+1]
                      * reading->current[i+1]));
    }

    vwSquare /= 3.0;
    iwSquare /= 3.0;
    pwSquare /= 3.0;

    edgeV = 0.5 * (float)reading->voltage[zc->vwIntg[0]]
                * (float)reading->voltage[zc->vwIntg[0]]
                * (float)zc->vwFrac[0]/4095.0;
    edgeI = 0.5 * (float)reading->current[zc->iwIntg[0]]
                * (float)reading->current[zc->iwIntg[0]]
                * (float)zc->iwFrac[0]/4095.0;
    edgeP = 0.5 * (float)reading->voltage[zc->vwIntg[0]]
                * (float)reading->current[zc->iwIntg[0]]
                * (float)zc->vwFrac[0]/4095.0;

    vwSquare += edgeV;
    iwSquare += edgeI;
    pwSquare += edgeP;

    edgeV = 0.5 * (float)reading->voltage[zc->vwIntg[1]]
                * (float)reading->voltage[zc->vwIntg[1]]
                * (float)zc->vwFrac[1]/4095.0;
    edgeI = 0.5 * (float)reading->current[zc->iwIntg[1]]
                * (float)reading->current[zc->iwIntg[1]]
                * (float)zc->iwFrac[1]/4095.0;
    edgeP = 0.5 * (float)reading->voltage[zc->vwIntg[1]]
                * (float)reading->current[zc->iwIntg[1]]
                * (float)zc->vwFrac[1]/4095.0;

    vwSquare += edgeV;
    iwSquare += edgeI;
    pwSquare += edgeP;

	// Compute vrms, irms, and ppwr
	output->vrms = sqrtf(vwSquare/periodpts);
	output->irms = sqrtf(iwSquare/periodpts);
	output->ppwr = pwSquare/periodpts;

	// Compute spwr, qpwr, freq, and phase difference
	output->spwr = output->vrms * output->irms;
	output->qpwr = sqrtf(output->spwr * output->spwr
	               - output->ppwr * output->ppwr);
	output->freq = (float)SAMPLING_FREQ/periodpts;

	return;
}
