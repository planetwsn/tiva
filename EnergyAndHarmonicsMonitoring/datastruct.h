/* Variable definition files
		Written by:
		Date:
*/
#ifndef DATASTRUCT_H_
#define DATASTRUCT_H_

#include <stdint.h>
#include <stdbool.h>

#define SAMPLESIZE      512 //1024
#define CORRECTING_GAIN             2000
#define CURRENT_CONDITIONING_FACTOR 3300.0/(614250.0*CORRECTING_GAIN)
#define VOLTAGE_CONDITIONING_FACTOR 121.0/(630.0*CORRECTING_GAIN)

#define VOLTAGE_GAIN_CALIBRATION    0.9810
#define VOLTAGE_OFFSET_CALIBRATION  -1.6563
#define CURRENT_GAIN_CALIBRATION    1.08
#define CURRENT_OFFSET_CALIBRATION  0.0044

typedef struct dtWaveform{
	float voltage[SAMPLESIZE];
	float current[SAMPLESIZE];
}dtWaveform_t, *dtWaveform_ptr;

typedef struct harmonicData{
    float r;
    float i;
}harmonicData_t, *harmonicData_ptr;

typedef struct tdData{
	float vrms;
	float irms;
	float ppwr;
	float qpwr;
	float spwr;
	float freq;
	float ph;
}tdData_t, *tdData_ptr;

typedef struct fdData{
	float vrms1;
	float irms1;
    float vrmsh;
    float irmsh;
    float thd;
}fdData_t, *fdData_ptr;

typedef struct zcData{ 
	uint32_t vwIntg[4];
	uint32_t vwFrac[4];
	uint32_t iwIntg[4];
	uint32_t iwFrac[4];
}zcData_t, *zcData_ptr;

enum sampleSequencer{
    SAMPLE_SEQUENCER0,
    SAMPLE_SEQUENCER1,
    SAMPLE_SEQUENCER2,
    SAMPLE_SEQUENCER3
};

enum priority{
    TOP_PRIORITY,
    FIRST_PRIORITY,
    SECOND_PRIORITY,
    THIRD_PRIORITY
};

/* GLOBAL VARIABLE */
bool isSampleReady;



#endif // DATASTRUCT_H_
